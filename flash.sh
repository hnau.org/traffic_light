set -e

avr-gcc -Wall -Os -mmcu=attiny13a main.c -o main.o
avr-objcopy -j .text -j .data -O ihex main.o main.hex
sudo avrdude -p t13a -c usbasp -U flash:w:main.hex:i
