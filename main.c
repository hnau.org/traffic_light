#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>

#define bool uint8_t
#define true 1
#define false 0

#define PIN_BUTTON PB1
#define PIN_YELLOW PB0
#define PIN_RED_GREEN_COMMON PB2
#define PIN_RED PB3
#define PIN_GREEN PB4

#define PROGRAM_LENGTH 8
#define PROGRAM_STEP_LENGTH 3
#define BIT_RED 0
#define BIT_YELLOW 1
#define BIT_GREEN 2
#define DESIBLED_LEDS_MASK 0b00000000
uint8_t program[PROGRAM_LENGTH] = {
	0b00000101,
	0b00000001,
	0b00000101,
	0b00000001,
	0b00000101,
	0b00000011,
	0b00000011,
	0b00000011
};

#define POWER_DOWN_STEP 5000

void setTimerEnabled(bool enabled) {
	if (enabled) {
		TCCR0B = (1<<CS02) | (0<<CS01) | (1<<CS00);
	} else {
		TCCR0B = 0;
	}
}

bool reversed = false;
void printLeds(
	uint8_t mask
) {
	bool yellow = (mask>>BIT_YELLOW) & 0b1;
	if (yellow) {
		PORTB |= 1<<PIN_YELLOW;
	} else {
		PORTB &= ~(1<<PIN_YELLOW);
	}
	if (reversed) {
		PORTB |= 1<<PIN_RED_GREEN_COMMON;
	} else {
		PORTB &= ~(1<<PIN_RED_GREEN_COMMON);
	}
	bool red = (mask>>BIT_RED) & 0b1;
	if (reversed != red) {
		PORTB |= 1<<PIN_RED;
	} else {
		PORTB &= ~(1<<PIN_RED);
	}
	bool green = (mask>>BIT_GREEN) & 0b1;
	if (reversed != green) {
		PORTB |= 1<<PIN_GREEN;
	} else {
		PORTB &= ~(1<<PIN_GREEN);
	}
}


volatile bool clicked = false;
ISR(INT0_vect) {
	clicked = true;
	setTimerEnabled(false);
	setTimerEnabled(true);
}


uint16_t powerDownWaitStep = 0;
uint8_t programStep = 0;
bool programIsRunning = false;
bool isSlipping = false;

ISR(TIM0_COMPA_vect) {
	
	if (clicked) {
		clicked = false;
		powerDownWaitStep = 0;
		
		if (isSlipping) {
			isSlipping = false;
			programStep = 0;
			programIsRunning = false;
		} else {
			programIsRunning = true;
		}
	}
	
	powerDownWaitStep++;
	if (powerDownWaitStep >= POWER_DOWN_STEP) {
		printLeds(DESIBLED_LEDS_MASK);
		setTimerEnabled(false);
		isSlipping = true;
		set_sleep_mode(SLEEP_MODE_PWR_DOWN);
		return;
	}
	
	if (programIsRunning) {
		programStep++;
		if (programStep >= PROGRAM_LENGTH * PROGRAM_STEP_LENGTH) {
			programStep = 0;
			programIsRunning = false;
			reversed = !reversed;
		}
	}
	
	printLeds(program[programStep / PROGRAM_STEP_LENGTH]);
	set_sleep_mode(SLEEP_MODE_PWR_DOWN);
}

int main() {
	ADCSRA &= ~(1<<ADEN);
	
	DDRB &= ~(1<<PIN_BUTTON);
	PORTB |= 1<<PIN_BUTTON;
	
	DDRB |= (1<<PIN_YELLOW) | (1<<PIN_RED_GREEN_COMMON) | (1<<PIN_RED) | (1<<PIN_GREEN);
	
	MCUCR |= (1<<ISC00) | (1<<ISC01);
	GIMSK |= 1<<INT0;
	
	TCNT0 = 0x00;
	OCR0A = 0xFF;
	TIMSK0 |= 1<<OCIE0A;
	setTimerEnabled(true);
  
	sei();
	
	while (1) {}
}
